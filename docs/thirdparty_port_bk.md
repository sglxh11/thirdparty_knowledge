# OpenHarmony C/C++三方库移植适配指导说明

## 简介

众所周知，C/C++三方库相对与JS/ETS的三方组件来说，其运行效率高。那如何将一个C/C++三方库移植到OH系统上呢？本文将介绍如何快速高效的移植一个C/C++三方库到OpenHarmony上。

## C/C++三方库移植适配流程

C/C++三方库移植适配流程主要包含原生库下载，三方库进行分析，编译构建的适配以及功能验证和供应用使用。

### 三方库依赖分析

三方库的分析包含以下几点：

- NDK依赖分析

  主要是指该库是否依赖Android NDK或是Android Games这类项目的依赖。

- C库运行时依赖分析
  
  OpenHarmony目前使用的是musl的C库，如果三方库中使用了bionic的C库的接口，而在musl的C库没有该接口，那移植该库则会存在适配该接口的风险。

- 其他三方库依赖分析
  
  其他的三方库依赖分析可以参照[如何贡献一个C/C++三方库 - 外部库依赖分析](https://gitee.com/openharmony-sig/knowledge/tree/master/docs/openharmony_getstarted/port_thirdparty#%E5%A4%96%E9%83%A8%E5%BA%93%E4%BE%9D%E8%B5%96%E5%88%86%E6%9E%90).

在依赖分析中，比较关键的阻塞点有以下几点：

- 北向应用C运行时受限
- 线程类函数受限
- 标准库支持有限
- 北向应用沙箱环境

针对这些问题我们的解决方案是：

- 基于敏感API清单进行静态扫描
- 基于运行时版本导出符号表确认
- 不断完善敏感API清单

根据当前的方案，我们已开发了对应的[C/C++三方库风险识别工具](https://gitee.com/han_jin_fei/e2e/tree/master/thirdparty_compare),通过该工具可以扫描出三方库是否有对NDK的依赖，是否有对OH API的依赖，是否有C库运行时依赖等。该工具可以让我们快速的对一个C/C++三方库进行风险识别。
工具的使用请参照[C/C++三方库风险识别工具使用方法](https://gitee.com/han_jin_fei/e2e/tree/master/thirdparty_compare#%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95).

### 三方库编译构建

解决完一个三方库的依赖分析后，那我们就该考虑该库在OpenHarmony上的编译构建了。OpenHarmony的应用编译开发使用的是[DevEco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio#download),而该工具目前只支持cmake的编译，但开源的C/C++三方库编译方式多样化，包含cmake,configured等方式。对于非cmake编译方式的三方库，我们需要分析该库的编译方式进行手写CMakeLists.txt文件将该库编译方式改为cmake编译构建，该过程比较耗时，尤其对一些大型的C/C++三方库，针对于该问题，我们开发一套基于linux下用原生库的编译脚本进行交叉编译三方库的工具[lycium](https://gitee.com/han_jin_fei/lycium).该工具协助开发者, 在 OpenHarmony 系统上快速编译、验证c/c++ 库.使用该工具，我们只需要配置一些三方库的基本信息，然后执行工具的编译脚本则可以完成该库的交叉编译。对于工具的使用，我们会在后面实例中详细讲解。

### 三方库功能验证

C/C++三方库功能验证是基于原生库的测试用例进行测试验证。为此，我们集成了一套可以在OH环境上进行make test等操作的环境:[CITools](https://gitee.com/han_jin_fei/lycium-citools),通过配置完这套环境，我们就可以在OH环境上运行原生库的测试用例了。

### 三方库的使用

将编译好的三方库以及对应的头文件拷贝到应用工程对应的`CPP`目录，或将三方库拷贝到对应的`工程/entry/libs/armxx/`目录，头文件拷贝到`工程/entry/include`目录。目前建议使用第一种方式，即将三方库以及对应的头文件拷贝到`CPP`目录。

## 快速适配三方库实例

lycium工具现在支持cmake以及config及make等构建方式，下面以minizip-ng三方库为例详细讲解lycium工具的使用。

### 工具下载

在社区下载[lycium工具](https://gitee.com/han_jin_fei/lycium)

``` shell
cd ~
git clone git@gitee.com:han_jin_fei/lycium.git
```

### 编译环境搭建

参照[lycium编译环境搭建](https://gitee.com/han_jin_fei/lycium/blob/master/Buildtools/README.md)

### 编译脚本的编写

三方库构建脚本规则：

```shell
# Contributor: Your Name <youremail@domain.com>
# Maintainer: Your Name <youremail@domain.com>

pkgname=NAME # 库名
pkgver=VERSION # 库版本
pkgrel=0 # 发布号
pkgdesc="" # 库描述
url="" # 官网链接
archs=("armeabi-v7a" "arm64-v8a") # cpu 架构
license=()
depends=() # 依赖库的目录名 当前库的依赖库，保证被依赖的库的archs是当前库的archs的超集
makedepends=() # 构建库时的依赖
install= # 安装路径
source="https://downloads.sourceforge.net/$pkgname/$pkgname-$pkgver.tar.gz" # 库源码下载链接

patchflag=false # 是否打patch 默认 false
autounpack=true # 是否自动解压默认 true, 如果为 false 则需要用户在 prepare 函数中自行解压
downloadpackage=true # 下载的是否是压缩包，默认 true, 一些特殊情况，代码需要代码只能 git clone (项目中依赖 submoudle )
buildtools= # 编译方法，暂时支持cmake configure，是什么就填写什么，如果不存在变量则默认为cmake。

builddir= # 源码压缩包解压后目录名
packageName=$builddir.tar.gz # 压缩包名

# 为编译做环境如设置环境变量，创建编译目录等
prepare() {
    cd $builddir
    cd ${OLDPWD}
}

# ${OHOS_SDK} oh sdk安装路径
# $ARCH 编译的架构是 archs 的遍历
# `pwd`/../../../$ARCH-install 安装到顶层目录的$ARCH-install
# 执行编译构建的命令
build() {
    cd $builddir
    ${OHOS_SDK}/native/build-tools/cmake/bin/cmake $* -DOHOS_ARCH=$ARCH -B$ARCH-build -S./ -L
    make -j4 -C $ARCH-build
    # 对最关键一步的退出码进行判断
    ret=$?
    cd $OLDPWD
    return $ret
}

# 安装打包
package() {
    cd $builddir
    make -C $ARCH-build install
    cd $OLDPWD
}

# 测试，需要在 ohos 设备上进行
check() {
    echo "Test must be on the OpenHarmony device!"
}

# 清理环境
cleanbuild() {
  rm -rf ${PWD}/$builddir #${PWD}/$packageName
}
```

通过对minizip-ng三方库的分析，我们可以知道，该库依赖了openssl，lzma,bzip2,libz,zstd以及libiconv等诸多其他的三方库，我们可以选择需要依赖的三方库，关闭不依赖的三方库，此例中我们选择lzma,openssl以及bzip2为依赖进行说明，而这3个依赖库的编译方式分别是cmake,configure以及make。

编译脚本可以参照[HPKBUILD 模板](https://gitee.com/han_jin_fei/lycium/blob/master/template/HPKBUILD)进行编写，该模板对于模板中的变量及函数做了详细说明。

- 在main目录下创建minizip-ng三方库目录，以及对应的xz(该库编译完后会生成liblzma.a以及对应的liblzma.so)，openssl和bzip2.
  
  ``` shell
  cd ~/lycium/main
  mkdir minizip-ng
  mkdir xz
  mkdir openssl
  mkdir bzip2
  ```

- 参照//lycium/template/下的HPKBUILD编写对应库的HPKBUILD脚本
- xz三方库的HPKBUILD脚本  <\br>
  xz库的编译构建方式是cmake编译构建，其脚本编写如下:

  ```shell
  # Contributor: Your Name <youremail@domain.com>
  # Maintainer: Your Name <youremail@domain.com>
  pkgname=xz
  pkgver=5.4.1
  pkgrel=0
  pkgdesc=""
  url=""
  archs=("armeabi-v7a" "arm64-v8a")
  license=("public domain" "LGPLv2.1" "GPLv2" "GPLv3")
  depends=()
  makedepends=()
  install=
  source="https://tukaani.org/$pkgname/$pkgname-$pkgver.tar.gz"

  autounpack=true
  downloadpackage=true

  builddir=$pkgname-${pkgver}
  packageName=$builddir.tar.gz

  prepare() {
    mkdir -p $builddir/$ARCH-build
    cd "$builddir"

    cd $OLDPWD # 1> /dev/null
  }

  build() {
    cd $builddir
    ${OHOS_SDK}/native/build-tools/cmake/bin/cmake $* -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DOHOS_ARCH=$ARCH -B$ARCH-build -S./ -L > `pwd`/$ARCH-build/build.log 2>&1
    make -j4 -C $ARCH-build >> `pwd`/$ARCH-build/build.log 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
  }

  package() {
    cd "$builddir"
    make -C $ARCH-build install >> `pwd`/$ARCH-build/build.log 2>&1
    cd $OLDPWD
  }

  check() {
    echo "Test MUST on OpenHarmony device!"
  }

  # 清理环境
  cleanbuild(){
    rm -rf ${PWD}/$builddir #${PWD}/$packageName
  }

  ```
  
  详细说明：

  1. 在模板文件的基础添加三方库的基本信息即可。该库的编译方式是cmake的，如果`buildtools`该变量未设置则默认该库编译方式为cmake。
  2. `prepare`函数是在编译前需要配置的一些信息如，环境变量等，cmake编译在编译时会设置toolchain，因此不需设置环境变量，此处只创建了一个编译目录。
  3. `build`函数是实现三方库编译函数，在该函数中，需要先执行`cmake`生成`Makefile`文件，然后在执行`make`命令进行编译。</br>
      在执行`cmake`时我们可以根据库的实际情况进行一些配置，如`-DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON`时配置该库编译类型为`Release`以及打开编译动态库的开关，`$*`是框架根据`buildtools`类型设置的一些通用配置.
  4. `package`函数是用来安装三方库的，一般的三方库都支持`make install`,该命令会将三方库生成的库文件与测试应用以及对应头文件安装到`//lycium/usr/三方库名-架构-install`目录下(如`xz`库安装路径：`//lycium/usr/xz-armeabi-v7ainstall`; `//lycium/usr/arm64-v8a`)。如果三方库不支持`install`的话，我们需要在该函数手动实现安装，将对应架构的三方库文件，测试用例以及头文件拷贝到`//lycium/usr/三方库名-架构-install`
  5. `check`函数实现测试时需要做的一些配置，如果无任何配置，该函数可为空。如果有配置，配置完后需要在该函数中注释说明最终测试的方法。
  6. `cleanbuild` 函数实现清理环境，主要删除编译目录，实现三方库的重新编译。

- openssl编译构建脚本HPKBUILD编写

  ``` shell
  # Contributor: Your Name <youremail@domain.com>
  # Maintainer: Your Name <youremail@domain.com>
  pkgname=openssl
  pkgver=OpenSSL_1_1_1t
  pkgrel=0
  pkgdesc=""
  url=""
  archs=("armeabi-v7a" "arm64-v8a")
  license="Apache License 2.0"
  depends=()
  makedepends=()
  install=
  source="https://github.com/openssl/$pkgname/archive/refs/tags/$pkgver.zip"

  autounpack=true
  downloadpackage=true
  buildtools="configure"

  builddir=$pkgname-${pkgver}
  packageName=$builddir.zip

  source envset.sh

  host=

  prepare() {
    mkdir -p $builddir/$ARCH-build
    if [ $ARCH == ${archs[0]} ]
    then
        setarm32ENV
        host=linux-generic32
    fi
    if [ $ARCH == ${archs[1]} ]
    then
        setarm64ENV
        host=linux-aarch64
    fi
  }

  #参数1
  build() {
    cd $builddir/$ARCH-build
    ../Configure $* $host > `pwd`/build.log 2>&1
    make -j4 >> `pwd`/build.log 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
  }

  package() {
    cd $builddir/$ARCH-build
    make install >> `pwd`/build.log 2>&1
    cd $OLDPWD
  }

  check() {
    cd $builddir/$ARCH-build
    make depend >> `pwd`/build.log 2>&1
    cd $OLDPWD
    if [ $ARCH == ${archs[0]} ]
    then
        unsetarm32ENV
    fi
    if [ $ARCH == ${archs[1]} ]
    then
        unsetarm64ENV
    fi
    unset host
    echo "Test MUST on OpenHarmony device!"
    # real test CMD
    # 将编译目录加到 LD_LIBRARY_PATH 环境变量
    # make test
  }

  # 清理环境
  cleanbuild(){
    rm -rf ${PWD}/$builddir #${PWD}/$packageName
  }
  ```

  详细说明：
  1. 该库编译构建方式是`configure`,必须指定`buildtools`变量
  2. `configure`方式是需要配置对应的环境变量，框架已封装对应的接口在`envset.sh`中，所以在调用相关接口前需要执行`source envset.sh`
  3. `configure`交叉编译还需要指定`host`，所以需要定义相关变量
  4. `prepare`函数在编译前需要根据架构设置对应的编译环境以及配置对应的`host`。
  5. 该库测试用例需要通过`make depend`生成，所以在`check`函数中需要执行`make depend`，并在该函数中对之前配置的环境变量进行清空，也需要将对应的变量在该函数中进行清空。

- bzip2三方库的HPKBUILD脚本
  
  ```shell
  # Contributor: Your Name <youremail@domain.com>
  # Maintainer: Your Name <youremail@domain.com>
  pkgname=bzip2
  pkgver=1.0.6
  pkgrel=0
  pkgdesc=""
  url=""
  archs=("armeabi-v7a" "arm64-v8a")
  license=("public domain" "LGPLv2.1" "GPLv2" "GPLv3")
  depends=()
  makedepends=()
  install=
  source="https://sourceforge.net/projects/$pkgname/files/$pkgname-$pkgver.tar.gz"

  autounpack=true
  downloadpackage=true
  buildtools="make"

  builddir=$pkgname-${pkgver}
  packageName=$builddir.tar.gz

  cc=
  ar=
  ranlib=
  # bzip2 采用makefile编译构建，为了保留构建环境(方便测试)。因此同一份源码在解压后分为两份,各自编译互不干扰
  prepare() {
    cp -rf $builddir $builddir-$ARCH-build
    cd $builddir-$ARCH-build
    if [ $ARCH == ${archs[0]} ]
    then
        cc=${OHOS_SDK}/native/llvm/bin/arm-linux-ohos-clang
        ar=${OHOS_SDK}/native/llvm/bin/llvm-ar
        ranlib=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
    fi
    if [ $ARCH == ${archs[1]} ]
    then
        cc=${OHOS_SDK}/native/llvm/bin/aarch64-linux-ohos-clang
        ar=${OHOS_SDK}/native/llvm/bin/llvm-ar
        ranlib=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
    fi
    cd $OLDPWD # 1> /dev/null
  }

  build() {
    cd $builddir-$ARCH-build
    make CC=${cc} AR=${ar} RANLIB=${ranlib} -j4 libbz2.a bzip2 bzip2recover > `pwd`/build.log 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
  }

  package() {
    cd $builddir-$ARCH-build
    make install PREFIX=$LYCIUM_ROOT/usr/$pkgname-$ARCH-install >> `pwd`/build.log 2>&1
    cd $OLDPWD
    unset cc ar ranlib
  }

  check() {
    echo "Test MUST on OpenHarmony device!"
    # make check
  }

  # 清理环境
  cleanbuild(){
    rm -rf ${PWD}/$builddir $builddir-armeabi-v7a-build  $builddir-arm64-v8a-build #${PWD}/$packageName
  }
  ```

  详细说明：
  1. `bzip2`原生库带有`Makefile`，其编译构建方式是直接执行`make`的, 对于这种库，我们只需配置`Makefile`中涉及到的编译变量即可，如果`Makefile`中的编译指令是写死的话，需要考虑该库是否支持交叉编译，支持的话我们可以通过`patch`方式修改`Makefile`中的编译指令。本库支持编译变量的设置，因此我们在脚本中配置对应的编译变量即可，不需要设置环境变量。
  2. 在使用完编译变量后需要将这些变量清空，一般在`package`函数中执行。

- minizip-ng三方库的HPKBUILD脚本
  
  ```shell
  # Contributor: Your Name <youremail@domain.com>
  # Maintainer: Your Name <youremail@domain.com>

  pkgname=minizip-ng
  pkgver=3.0.4
  pkgrel=0
  pkgdesc=""
  url=""
  archs=("armeabi-v7a" "arm64-v8a") 
  license=("zlib")
  depends=("openssl" "xz" "bzip2")
  makedepends=()
  install=
  source="https://github.com/zlib-ng/${pkgname}/archive/refs/tags/${pkgver}.tar.gz"

  patchflag=false
  autounpack=true
  downloadpackage=true
  buildtools="cmake"

  builddir=$pkgname-${pkgver}
  packageName=$builddir.tar.gz

  # 为编译做环境如设置环境变量，创建编译目录等
  prepare() {
    mkdir -p $builddir/$ARCH-build
  }

  # 执行编译构建的命令
  build() {
    cd $builddir
    ## 此处将依赖库iconv以及lzma屏蔽，如有强制需求可以将此2库定义为true并在depends中加上这2个库的依赖
    PKG_CONFIG_PATH=${LYCIUM_ROOT}/usr/openssl-${ARCH}-install/lib/pkgconfig ${OHOS_SDK}/native/build-tools/cmake/bin/cmake $* -DMZ_ZSTD=false -DMZ_ICONV=false -DOHOS_ARCH=$ARCH -B$ARCH-build -S./ -L > `pwd`/$ARCH-build/build.log 2>&1
    make -j4 -C $ARCH-build >> `pwd`/$ARCH-build/build.log 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
  }

  # 安装打包
  package() {
    cd $builddir
    make -C $ARCH-build install
    cd $OLDPWD
  }

  # 测试，需要在 ohos 设备上进行
  check() {
    echo "Test MUST on OpenHarmony device!"
  }

  # 清理环境
  cleanbuild(){
    rm -rf ${PWD}/$builddir #${PWD}/$packageName
  }
  ```

  详细说明：

  1. `minizip-ng`三方库编译构建方式是`cmake`，与`xz`不同的是，该库是有依赖的，所以在`depends`中我们需要填写上依赖的库名称。
  2. 编译时我们还需要指定对应依赖库的查找路径，框架会根据`buildtools`方式传入对应的库一些通用配置，包含了`cmake`方式编译的库的依赖查找路径`CMAKE_FIND_ROOT_PATH`,但是如果依赖库是`configrue`的话，该方式无法准确的搜索到依赖库的路径，因此对于依赖`configrue`生成的库，我们需要配置`PKG_CONFIG_PATH`，该路径需要指定到库的`pkgconfig`路径。
  3. 在进行`cmake`时，除了配置依赖库的查找路径，还关闭了一些不需要依赖的库，如`-DMZ_ZSTD=false -DMZ_ICONV=false`设置该库不依赖`zstd`和`iconv`.

  其他依赖库根据原生库编译方式参照以上2种方式进行编写`HPKBUILD`文件即可。其他更多的用例可参照[lycium框架main](https://gitee.com/han_jin_fei/lycium/tree/master/main)下已移植的三方库。

### 编译构建

编写完HPKBUILD文件后，我们就可以进行三方库的编译了。在`lycium`的根目录执行如下指令进行编译：

```shell
./build.sh minizip-ng xz bzip2 openssl
```

其中minizip-ng是我们需要编译的三方库，而zstd,bzip2以及openssl是minizip-ng三方库的依赖库。进行编译完后，会在`lycium/usr`下生成对应架构的三方库的的库文件和头文件等。

![install path](media/install_path.png)

注意：

- 有依赖库的必须将依赖库一起编译，否则框架无法进行编译。
- 安装目录下对应的三方库有2份，其中armeabi-v7a对应的32位的库，arm64-v8a对应的是64位的库。

### 应用使用

将对应框架下的依赖库`libminizip.a`以及其依赖的其他三方库文件和头文件拷贝到`minizip`工程的`cpp`目录下,并修改`CMakeLists.txt`文件，添加`include`路径以及`link libraaries`路径，如下图所示：

![minizip project](media/minizip_pro.png)

然后通过 `Build`->`Rebuild Project`，然后就能看到编译成功：

![build OK](media/minizip_build.png)

## 参考文档

- [lycium工具仓库](https://gitee.com/han_jin_fei/lycium/tree/master)
- [C/C++三方库风险识别工具](https://gitee.com/han_jin_fei/e2e/tree/master/thirdparty_compare)
- [minizip Demo](https://gitee.com/zhong-luping/knowledge_demo_temp/tree/master/FA/thirdparty/minizip_demo)
