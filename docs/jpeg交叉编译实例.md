# jpeg交叉编译实例

## 编译前准备

### OHOS SDK准备

1. 从 openHarmony SDK [官方发布渠道](https://gitee.com/openharmony-sig/oh-inner-release-management/blob/master/Release-Testing-Version.md) 下载SDK
2. 解压SDK

   ```shell
   owner@ubuntu:~/workspace$ tar -zxvf version-Master_Version-OpenHarmony_3.2.10.3-20230105_163913-ohos-sdk-full.tar.gz
   ```
  
  进入到sdk的linux目录，解压native工具：

  ```shell
  owner@ubuntu:~/workspace$ cd ohos_sdk/linux
  owner@ubuntu:~/workspace/ohos-sdk/linux$ for i in *.zip;do unzip ${i};done
  owner@ubuntu:~/workspace/ohos-sdk/linux$ ls
  ets                                native                                   toolchains
  ets-linux-x64-4.0.1.2-Canary1.zip  native-linux-x64-4.0.1.2-Canary1.zip     toolchains-linux-x64-4.0.1.2-Canary1.zip
  js                                 previewer
  js-linux-x64-4.0.1.2-Canary1.zip   previewer-linux-x64-4.0.1.2-Canary1.zip
  ```

### 编译环境准备

jpeg的编译构建方式是configure，需要配置对应的环境变量,本示例以ohos 64bit进行配置，如需编译32bit的库，请更改32bit配置信息：

1. 配置`OHOS_SDK`路径

   ```shell
   owner@ubuntu:~/workspace$ export OHOS_SDK=/home/owner/tools/OHOS_SDK/ohos-sdk/linux/                  ## 此处配置成自己的sdk解压目录
   ```

2. 配置编译命令,在命令行输入以下命令：

  ```shell
  export AS=${OHOS_SDK}/native/llvm/bin/llvm-as
  export CC="${OHOS_SDK}/native/llvm/bin/clang --target=aarch64-linux-ohos"         ## 32bit的target需要配置成 --target=arm-linux-ohos
  export CXX="${OHOS_SDK}/native/llvm/bin/clang++ --target=aarch64-linux-ohos"      ## 32bit的target需要配置成 --target=arm-linux-ohos
  export LD=${OHOS_SDK}/native/llvm/bin/ld.lld
  export STRIP=${OHOS_SDK}/native/llvm/bin/llvm-strip
  export RANLIB=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
  export OBJDUMP=${OHOS_SDK}/native/llvm/bin/llvm-objdump
  export OBJCOPY=${OHOS_SDK}/native/llvm/bin/llvm-objcopy
  export NM=${OHOS_SDK}/native/llvm/bin/llvm-nm
  export AR=${OHOS_SDK}/native/llvm/bin/llvm-ar
  export CFLAGS="-fPIC -D__MUSL__=1"                                            ## 32bit需要增加配置 -march=armv7a
  export CXXFLAGS="-fPIC -D__MUSL__=1"                                          ## 32bit需要增加配置 -march=armv7a
  ```

### 三方库源码准备

下载三方库代码：

```shell
owner@ubuntu:~/workspace$ wget http://www.ijg.org/files/jpegsrc.v9e.tar.gz       ## 下载指定版本的源码包
owner@ubuntu:~/workspace$ tar -zxvf jpegsrc.v9e.tar.gz                           ## 解压源码包
```

## 编译 & 安装

- 进入到jpeg目录，执行configure构建脚本

**注意：configure时必须配置交叉编译选项host,否则无法交叉编译成功，prefix配置编译完后安装路径，不配置该选项，编译完后执行安装默认安装到系统`/usr/`**

```shell
owner@ubuntu:~/workspace/jpeg-9e$ ./configure --prefix=/home/owner/workspace/usr/jpeg --host=aarch64-linux
checking build system type... x86_64-pc-linux-gnu
checking host system type... x86_64-pc-linux-gnu
checking target system type... x86_64-pc-linux-gnu
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a race-free mkdir -p... /usr/bin/mkdir -p
...
...
...
configure: creating ./config.status
config.status: creating Makefile
config.status: creating libjpeg.pc
config.status: creating jconfig.h
config.status: executing depfiles commands
config.status: executing libtool commands
```

- configure构建成功后执行make

```shell
owner@ubuntu:~/workspace/jpeg-9e$ make
make  all-am
make[1]: Entering directory '/home/owner/workspace/jpeg-9e'
  CC       cjpeg.o
  CC       rdppm.o
  CC       rdgif.o
  CC       rdtarga.o
  ...
  ...
  ...
  CC       rdcolmap.o
  CCLD     djpeg
  CC       jpegtran.o
  CC       transupp.o
  CCLD     jpegtran
  CC       rdjpgcom.o
  CCLD     rdjpgcom
  CC       wrjpgcom.o
  CCLD     wrjpgcom
make[1]: Leaving directory '/home/owner/workspace/jpeg-9e'
```

- 执行安装命令

```shell
owner@ubuntu:~/workspace/jpeg-9e$ make install
```

执行完后对应的文件安装到prefix配置的路径`/home/owner/workspace/usr/jpeg`, 查看对应文件属性：

```shell
owner@ubuntu:~/workspace/jpeg-9e$ cd /home/owner/workspace/usr/jpeg
owner@ubuntu:~/workspace/usr/jpeg$ ls
bin  include  lib  share
owner@ubuntu:~/workspace/usr/jpeg$ ls lib
libjpeg.a  libjpeg.la  libjpeg.so  libjpeg.so.9  libjpeg.so.9.5.0  pkgconfig
owner@ubuntu:~/workspace/usr/jpeg$ ls include/
jconfig.h  jerror.h  jmorecfg.h  jpeglib.h
owner@ubuntu:~/workspace/usr/jpeg$ file lib/libjpeg.so.9.5.0
lib/libjpeg.so.9.5.0: ELF 64-bit LSB shared object, ARM aarch64, version 1 (SYSV), dynamically linked, with debug_info, not stripped
```

## 测试

交叉编译完后，该测试验证我们的三方库。为了保证原生库功能完整，我们基于原生库的测试用例进行测试验证。为此，我们需要集成了一套可以在OH环境上进行cmake, ctest等操作的环境，具体请阅读 [测试环境配置文档](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/lycium/CItools/README_zh.md)。

1. 测试环境配置 <br>
   请参考 [测试环境配置文档](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/lycium/CItools/README_zh.md)。
2. 打包测试资源 <br>
   使用原生库的测试用例进行测试，我们为了保证测试时不进行编译操作，我们需要把整个编译的源码作为测试资源包推送到开发板,且需要保证三方库在开发板的路径与编译时路径一致：

   ```shell
   owner@ubuntu:~/workspace$ tar -zcf jpeg-9e.tar.gz jpeg-9e/
   ```

3. 将测试资源推送到开发板 <br>
打开命令行工具，执行[hdc_std](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-toolchain-hdc-guide.md#%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87)命令进行文件传输：

```shell
hdc_std file send X:\workspace\jpeg-9e.tar.gz /data/    ## 推送资源到开发板
hdc_std shell                                           ## 进入开发板系统
# mkdir -p /home/owner/                                 ## 设置与编译时同样的路径
# cd /home/owner/
# ln -s workspace /data/                                ## 系统根目录空间有限，建议通过软链接配置路径
# cd workspace
# tar -zxf jpeg-9e.tar.gz                               ## 解压测试资源
```

4. 执行测试

进入到三方库编译路径,执行`make check-local`进行测试：

```shell
# cd /home/owner/workspace/jpeg-9e                                         ## 进入测试目录
#  make check-local                                                        ## 执行测试命令
rm -f testout*
./djpeg -dct int -ppm -outfile testout.ppm ./testorig.jpg
./djpeg -dct int -gif -outfile testout.gif ./testorig.jpg
./djpeg -dct int -bmp -colors 256 -outfile testout.bmp ./testorig.jpg
./cjpeg -dct int -outfile testout.jpg ./testimg.ppm
./djpeg -dct int -ppm -outfile testoutp.ppm ./testprog.jpg
./cjpeg -dct int -progressive -opt -outfile testoutp.jpg ./testimg.ppm
./jpegtran -outfile testoutt.jpg ./testprog.jpg
cmp ./testimg.ppm testout.ppm
cmp ./testimg.gif testout.gif
cmp ./testimg.bmp testout.bmp
cmp ./testimg.jpg testout.jpg
cmp ./testimg.ppm testoutp.ppm
cmp ./testimgp.jpg testoutp.jpg
cmp ./testorig.jpg testoutt.jpg
```

测试结果中未出现任何错误提示，说明此测试用例执行通过。
