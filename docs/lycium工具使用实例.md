# lycium使用实例

## 工具下载

`lycium`工具存放在[tpc_c_cplusplus](https://gitee.com/openharmony-sig/tpc_c_cplusplus)仓，并配合此仓库文件目录结构进行快速编译，因此我们需要下载整个仓库：

```shell
owner@ubuntu:~/workspace$ git clone https://gitee.com/openharmony-sig/tpc_c_cplusplus.git
```

## 编译环境准备

1. 配置环境变量

配置SDK环境变量，该环境变量必须是native层的父目录，如下：

```shell
export OHOS_SDK=/home/owner/workspace/ohos-sdk/linux    ## 此处配置成自己的sdk解压目录
```

2. 拷贝编译工具

进入到`lycium`工具中的`Buildtools`目录，解压工具包并将工具拷贝到SDK目录下：

```shell
owner@ubuntu:~/workspace/tpc_c_cplusplus$ cd lycium/Buildtools/
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium/Buildtools$ ls                              # 查看当前文件
README.md  SHA512SUM  toolchain.tar.gz
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium/Buildtools$ sha512sum -c SHA512SUM          # 可校验工具包是否正常
toolchain.tar.gz: OK                                                                        # 该提示说明工具包是正常的
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium/Buildtools$ tar -zxf toolchain.tar.gz       # 解压工具包
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium/Buildtools$ ls
README.md  SHA512SUM  toolchain  toolchain.tar.gz
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium/Buildtools$ cp toolchain/* ${OHOS_SDK}/native/llvm/bin  # 拷贝工具到SDK对应的bin目录
```

3. 设置编译机cmake识别OHOS系统

由于sdk中的cmake版本过低, 导致很多开源库在cmake阶段报错. 这个时候就需要用户在编译机上安装一个高版本的cmake(推荐使用3.26及以上版本). 但是 cmake官方是不支持OHOS的. 解决方案:

```shell
cp $OHOS_SDK/native/build-tools/cmake/share/cmake-3.16/Modules/Platform/OHOS.cmake xxx(代表你编译机安装的cmake的路径)/cmake-3.26.3-linux-x86_64/share/cmake-3.26/Modules/Platform
```

## 修改三方库的编译方式以及编译参数

`lycium`框架提供了`HPKBUILD`文件供开发者对相应的C/C++三方库的编译配置。具体方法：

1. 在tpc_c_cplusplus/thirdparty目录下新建需要共建的三方库名字pkgname,本示例为cJSON.

   ```shell
   owner@ubuntu:~/workspace/tpc_c_cplusplus$ mkdir thirdparty/cJSON
   ```

2. 将lycium/template/目录下的HPKBUILD模板文件拷贝到新建三方库目录下:

   ```shell
   owner@ubuntu:~/workspace/tpc_c_cplusplus$ cp lycium/template/HPKBUILD thirdparty/cJSON
   ```

3. 根据三方库实际情况修改HPKBUILD模板:

    **注意：对于贡献的文件，需删除多余的注释**

   ```shell
    # Contributor: Your Name <youremail@domain.com>
    # Maintainer: Your Name <youremail@domain.com>

    pkgname=cJSON   # 库名
    pkgver=v1.7.15  # 库版本
    pkgrel=0
    pkgdesc=""
    url=""
    archs=("armeabi-v7a" "arm64-v8a")
    license=("MIT")
    depends=()
    makedepends=()

    source="https://github.com/DaveGamble/$pkgname/archive/refs/tags/$pkgver.tar.gz"    # 库源码下载链接

    autounpack=true
    downloadpackage=true
    buildtools=cmake                # 编译方法, 暂时支持cmake, configure, make等, 是什么就填写什么. 如若不写默认为cmake.
    builddir=$pkgname-${pkgver:1}   # 源码压缩包解压后目录名 编译目录名
    packagename=$builddir.tar.gz

    # 为编译设置环境，如设置环境变量，创建编译目录等
    prepare() {
        mkdir -p $builddir/$ARCH-build
    }
    # 执行编译构建的命令
    build() {
        cd $builddir
        ${OHOS_SDK}/native/build-tools/cmake/bin/cmake "$@" -B$ARCH-build -S./ -L > `pwd`/$ARCH-build/build.log 2>&1
        $MAKE -C $ARCH-build >> `pwd`/$ARCH-build/build.log 2>&1
        ret=$?
        cd $OLDPWD
        return $ret
    }
    # 打包安装
    package() {
        cd $builddir
        $MAKE -C $ARCH-build install >> `pwd`/$ARCH-build/build.log 2>&1
        cd $OLDPWD
    }
    # 进行测试的准备和说明
    check() {
        echo "The test must be on an OpenHarmony device!"
        # real test CMD
        # ctest
    }

    # 清理环境
    cleanbuild(){
        rm -rf ${PWD}/$builddir #${PWD}/$packagename
    }
   ```

## 快速编译三方库

配置完三方库的编译方式参数后，在lycium目录执行./build.sh cJSON，进行自动编译三方库，并打包安装到 usr/cJSON/
ARCH 目录

```shell
owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium$ ./build.sh cJSON       # 执行编译
Build OS linux
OHOS_SDK=/home/owner/ohos-sdk/linux/
CLANG_VERSION=15.0.4
gcc 命令已安装
cmake 命令已安装
make 命令已安装
pkg-config 命令已安装
autoreconf 命令已安装
patch 命令已安装
创建 /home/owner/workspace/tpc_c_cplusplus/lycium/usr 目录
Build cJSON v1.7.15 strat!
/home/owner/workspace/tpc_c_cplusplus/thirdparty/cJSON/cJSON-1.7.15.tar.gz，存在
cJSON-1.7.15.tar.gz: OK
/home/owner/workspace/tpc_c_cplusplus/thirdparty/cJSON/cJSON-1.7.15.tar.gz
Compileing OpenHarmony armeabi-v7a cJSON v1.7.15 libs...
The test must be on an OpenHarmony device!
Compileing OpenHarmony arm64-v8a cJSON v1.7.15 libs...
The test must be on an OpenHarmony device!
Build cJSON v1.7.15 end!
ALL JOBS DONE!!!                                                # 显示所有的编译完成

owner@ubuntu:~/workspace/tpc_c_cplusplus/lycium$ ls usr/cJSON/  # 查看安装的文件
arm64-v8a  armeabi-v7a

```

## 快速验证三方库

1. 测试环境准备<br>
   请参考 [lycium CItools](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/lycium/CItools/README_zh.md)。
2. 准备测试资源<br>
   使用原生库的测试用例进行测试，我们为了保证测试时不进行编译操作，我们需要把整个编译的源码作为测试资源包推送到开发板,且需要保证三方库在开发板的路径与编译时路径一致：

   ```shell
   owner@ubuntu:~/workspace$ tar -zcf tpc_c_cplusplus.tar.gz tpc_c_cplusplus        # 打包整个仓库资源(为了保持测试路径与编译路径一致，也可单独打包cJSON目录，但需逐层创建对应目录)
   ```

   打包完资源后需要将资源通过[hdc_std](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-toolchain-hdc-guide.md#%E7%8E%AF%E5%A2%83%E5%87%86%E5%A4%87)工具将资源包推送到开发板

   ```shell
   hdc_std file send X:\workspace\tpc_c_cplusplus.tar.gz /data/    ## 推送资源到开发板
   hdc_std shell                                           ## 进入开发板系统
   # mkdir -p /home/owner/                                 ## 设置与编译时同样的路径
   # cd /home/owner/
   # ln -s workspace /data/                                ## 系统根目录空间有限，建议通过软链接配置路径
   # cd workspace
   # tar -zxf tpc_c_cplusplus.tar.gz                               ## 解压测试资源
   ```

3. 执行测试

```shell
hdc_std shell                                                           ## 进入开发板系统
# cd /home/owner/workspace/tpc_c_cplusplus/thirdparty/cJSON             ## 进入到三方库目录
# export LD_LIBRARY_PATH=`pwd`/                                         ## 配置测试环境变量
# ctest                                                                 ## 执行原库的测试用例
Test project /data/workspace/tpc_c_cplusplus/thirdparty/cJSON/cJSON-1.7.15/arm64-v8a-build
      Start  1: cJSON_test
 1/19 Test  #1: cJSON_test .......................   Passed    0.02 sec
      Start  2: parse_examples
 2/19 Test  #2: parse_examples ...................   Passed    0.02 sec
      Start  3: parse_number
 3/19 Test  #3: parse_number .....................   Passed    0.02 sec
      Start  4: parse_hex4
 4/19 Test  #4: parse_hex4 .......................   Passed    0.07 sec
      Start  5: parse_string
 5/19 Test  #5: parse_string .....................   Passed    0.01 sec
      Start  6: parse_array
 6/19 Test  #6: parse_array ......................   Passed    0.01 sec
      Start  7: parse_object
 7/19 Test  #7: parse_object .....................   Passed    0.01 sec
      Start  8: parse_value
 8/19 Test  #8: parse_value ......................   Passed    0.01 sec
      Start  9: print_string
 9/19 Test  #9: print_string .....................   Passed    0.01 sec
      Start 10: print_number
10/19 Test #10: print_number .....................   Passed    0.01 sec
      Start 11: print_array
11/19 Test #11: print_array ......................   Passed    0.01 sec
      Start 12: print_object
12/19 Test #12: print_object .....................   Passed    0.01 sec
      Start 13: print_value
13/19 Test #13: print_value ......................   Passed    0.02 sec
      Start 14: misc_tests
14/19 Test #14: misc_tests .......................   Passed    0.01 sec
      Start 15: parse_with_opts
15/19 Test #15: parse_with_opts ..................   Passed    0.01 sec
      Start 16: compare_tests
16/19 Test #16: compare_tests ....................   Passed    0.01 sec
      Start 17: cjson_add
17/19 Test #17: cjson_add ........................   Passed    0.01 sec
      Start 18: readme_examples
18/19 Test #18: readme_examples ..................   Passed    0.01 sec
      Start 19: minify_tests
19/19 Test #19: minify_tests .....................   Passed    0.01 sec

100% tests passed, 0 tests failed out of 19                                 ## 测试结果

Total Test time (real) =   0.40 sec
```
