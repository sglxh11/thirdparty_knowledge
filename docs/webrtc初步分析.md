# WebRTC初步分析

## 源码分析

### webrtc整体的文件分析

- webrtc全量代码量：28754135
  三方库代码量(源码目录的thirdparty下有300个三方库)：24206571
  去除三方库后的代码总量：4547564

- 所有BUILD.gn文件: 1102
- 所有源码文件：<br>
  cc文件：18159 <br>
  c文件：7038 <br>
  h文件：127656

### 涉及到平台相关的文件分析(是否需要适配对应OHOS平台)

- BUILD.gn文件
  
  BUILD.gn文件中通过is_android/id_mac等控制模块编译选项 <br>
  1. 当前涉及android分支的gn文件：147
  
    涉及的模块包含：<br>
    api <br>
    base  <br>
    build <br>
    buildtools  <br>
    common_video  <br>
    common_audio  <br>
    examples  <br>
    logging <br>
    pc  <br>
    modules  <br>
    rtc_base  <br>
    rtc_tools  <br>
    sdk  <br>
    stats  <br>
    system_wrappers  <br>
    test  <br>
    testing  <br>
    tools  <br>
    third_party  <br>

  2. 当前涉及mac分支的gn文件：69

    涉及的模块包含：  <br>
    api  <br>
    base  <br>
    build  <br>
    examples  <br>
    modules  <br>
    rtc_base  <br>
    test  <br>
    testing  <br>
    third_party  <br>
    video  <br>

- 源码文件
  
  源码文件包含cc/c/h，其通过WEBRTC_ANDROID/WEBRTC_MAC等变量控制适配各平台的源码。<br>
  当前涉及android分支的gn文件：146  <br>
  当前涉及mac分支的gn文件：43

### 模块分析

- audio模块
  
  1）对应平台适配audio情况

  方案一. 对标MAC适配,在modules/audio_device/目录下适配ohos平台相关能力代码(适配代码量: 2835)

  | WEBRTC标准接口       | 接口能力说明             | MAC平台实现接口                                              | OH平台调用实现接口 |
  | -------------------- | ------------------------ | ------------------------------------------------------------ | ------------------ |
  | PlayoutDevices       | 获取播放设备数           | AudioObjectGetPropertyData                                   |                    |
  | RecordingDevices     | 获取录制设备数           | AudioObjectGetPropertyData                                   |                    |
  | PlayoutDeviceName    | 获取指定索引号播放设备名 | AudioObjectSetPropertyData                                   |                    |
  | RecordingDeviceName  | 获取指定索引号录制设备名 | AudioObjectSetPropertyData                                   |                    |
  | SetPlayoutDevice     | 通过索引号指定播放设备   | AudioObjectSetPropertyData                                   |                    |
  | SetRecordingDevice   | 通过索引号指定录制设备   | AudioObjectSetPropertyData                                   |                    |
  | PlayoutIsAvailable   | 当前播放设备是否有效     | AudioObjectGetPropertyData                                   |                    |
  | InitPlayout          | 初始化播放               | AudioObjectHasProperty、AudioObjectGetPropertyData、SetDesiredPlayoutFormat、AudioObjectAddPropertyListener、AudioDeviceCreateIOProcID |                    |
  | RecordingIsAvailable | 当前录制设备是否有效     | AudioObjectGetPropertyData                                   |                    |
  | InitRecording        | 初始化录音               | AudioObjectGetPropertyData、<br />AudioConverterNew、<br />AudioObjectSetPropertyData、<br />AudioObjectAddPropertyListener、<br />AudioDeviceCreateIOProcID |                    |
  | StartPlayout         | 开始播放                 | AudioDeviceStart                                             |                    |
  | StartRecording       | 开始录音                 | AudioDeviceStart                                             |                    |
  | StopPlayout          | 停止播放                 | AudioDeviceStop、<br />AudioDeviceDestroyIOProcID、<br />AudioConverterDispose、<br />AudioObjectHasProperty、<br />AudioObjectRemovePropertyListener |                    |
  | StopRecording        | 停止录音                 | AudioDeviceStop、<br />AudioDeviceDestroyIOProcID、<br />AudioConverterDispose、<br />AudioObjectRemovePropertyListener |                    |
  | InitSpeaker          | 初始化播放设备           | AudioObjectGetPropertyDataSize、<br />AudioObjectGetPropertyData |                    |
  | InitMicrophone       | 初始化录制设备           | AudioObjectGetPropertyDataSize、<br />AudioObjectGetPropertyData |                    |
  | SetSpeakerVolume     | 设备播放音量             | AudioObjectIsPropertySettable、<br />AudioObjectSetPropertyData |                    |
  | SpeakerVolume        | 获取播放音量             | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | SetMicrophoneVolume  | 设置录音音量             | AudioObjectIsPropertySettable、<br />AudioObjectSetPropertyData |                    |
  | MicrophoneVolume     | 获取录音音量             | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | SetSpeakerMute       | 设置播放静音状态         | AudioObjectIsPropertySettable、<br />AudioObjectSetPropertyData |                    |
  | SpeakerMute          | 获取播放静音状态         | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | SetMicrophoneMute    | 设置录制静音状态         | AudioObjectIsPropertySettable、<br />AudioObjectSetPropertyData |                    |
  | MicrophoneMute       | 获取录制静音状态         | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | SetStereoPlayout     | 设置播放双声道           | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | StereoPlayout        | 获取播放是否双声道       | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | SetStereoRecording   | 设置录制双声道           | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | StereoRecording      | 获取录制是否双声道       | AudioObjectIsPropertySettable、<br />AudioObjectGetPropertyData |                    |
  | PlayoutDelay         | 设置播放延时             | -                                                            |                    |

  方案二. 对标android实现分析

  需分析源码中android SDK具体实现代码，需要考虑适配OHOS对应的SDK框架(需媒体专家进行专业分析)

  SDK下android对应的audio_device模块代码量: 3034  <br />
                    audio_device_module模块代码量： 211 <br />
                    codecs模块代码量：53  <br />
  SDK下android对应媒体控制的能力由java代码实现，代码量：1589  <br />
  SDK下android提供了适配audio能力的api接口，由java实现，代码量：300

  2）各API对应OHOS平台是否需要适配对应接口

     api模块下有对mac及android做分支处理，需要详细解读对应代码

- vedio模块

  方案一. 对标linux适配，在modules/video_captrue目录下适配ohos平台相关能力代码

  需实现webrtc提供的标准类DeviceInfoImpl与VideoCaptureImpl

  linux实现代码量：1926

  方案二. 对标MAC/Android实现，在SDK中集成video采集渲染能力

  需分析源码中android SDK具体实现代码，需要考虑适配OHOS对应的SDK框架(需媒体专家进行专业分析)

